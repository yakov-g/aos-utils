Kafka
=====

Configs last tested with Kafka v**2.11-0.10.1.1**

# Configs
## server.properties

* UPDATE THIS*: Change value for key *log.dirs* to something unique to you. Directory permissions
issues may arise otherwise.
* Added partition.assignment.strategy = roundrobin, which ensures incoming data has a 
distribution method.
* Added delete.topic.enable = true, ensuring that Kafka will delete all local stores for a 
given topic once you call delete on it.

## zookeeper.properties

* UPDATE THIS*: Change value for key *dataDir* to something unique to you. Directory permissions
issues may arise otherwise.

# Utils
