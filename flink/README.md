Flink
=====

Configs last tested with Flink v**1.03**

# Configs
## flink-conf.yaml

* Edit value for key *taskmanager.numberOfTaskSlots:* to be equal to the number of cores each node
 has.
* Edit value for key *taskmanager.network.numberOfBuffers:* to be equal to the following formula:
 (4 * (# of TaskManagers) * (# slots per TaskManager ^ 2) )
* Edit value for key *jobmanager.rpc.address:* to the node that you'd like the Flink Job Manager to
run on.
 

# Utils
