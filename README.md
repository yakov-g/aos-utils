AOS-Utils
=========

This repository contains various scripts, utils and configs for Apache Open Source projects that we use interally at PM.

Navigation
----------

The current layout looks a bit like this:

> ├── README.md
> ├── flink
> │   ├── README.md
> │   ├── config
> │   ├── flink-conf.yaml
> │   └── flink-conf.yaml.default
> ├── kafka
> │   ├── README.md
> │   └── config
> │       ├── server.properties
> │       ├── server.properties.default
> │       └── zookeeper.properties.default
> ├── maven
> ├── spark
> └── zookeeper

Each project is listed under its own directory, and each project should contain a README.md in its root folder.  With that project folder should be:

* config - Default and modified configs
* util - scripts that are useful when running these projects
* misc - catch-all for other undefined instances.

Please try to adhere to this format if at all possible.
Thanks!