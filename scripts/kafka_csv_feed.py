#!/usr/bin/env python3
#Usage: ./kafka_csv_feed.py -f ~/household_power_consumption.csv --kafka daenerys-c19:9092 --topic KMEANS_TRAINING

import argparse
import time
from kafka_latency_prod import PMProducer

def main(options, host=None, port=None):
    filename=options.in_file
    topic=options.topic
    iters=options.iters
    flush=options.flush
    sleep_time=options.sleep
    append_timestamp=options.append_timestamp
    batch_size=options.batch
    desired_rate=options.rate

    PM_PROD = PMProducer(flush=flush)
    PM_PROD.kafka_prod_add(host, port, topic, prepend_label=False)

    i = 0
    while 1:
        batch_list = []
        start_time = None
        end_time = None
        with open(filename) as f:
            for line in f:
                if len(batch_list) == 0:
                    start_time = time.time()

                line = line.strip()
                if append_timestamp:
                    line += '`'
                    line = line + str(int(time.time() * 1000))

                batch_list.append(line)
                if len(batch_list) < batch_size:
                    continue

                for l in batch_list:
                    PM_PROD.send(l.encode("utf-8"))
                batch_list = []

                end_time = time.time()
                send_time = end_time - start_time
                cur_rate = batch_size  / (send_time)
                print("Rate(send): {0} samples / sec".format(round(cur_rate)))

                if desired_rate > 0:
                    if cur_rate > desired_rate:
                        sleep_time = batch_size / desired_rate - send_time
                    else:
                        sleep_time = 0

                if sleep_time != 0:
                    time.sleep(sleep_time)
                end_time = time.time()
                cur_rate = batch_size  / (end_time - start_time)
                print("Rate(send + sleep): {0} samples / sec".format(round(cur_rate)))
                if (cur_rate < 0.9 * desired_rate):
                    print("Warning: current rate is less then desired, try to increase batch size")

        for l in batch_list:
            PM_PROD.send(l.encode("utf-8"))
        batch_list = []

        i += 1
        if i == iters:
            break

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--iters", type=int, default=1,
                        help="number of times to feed the file (-1 for infinite)")

    parser.add_argument("-f", "--in_file", required=True,
                        help="input file")

    parser.add_argument("--kafka", required=True,
            help="host:port")

    parser.add_argument("--topic", required=True,
            help="topic")

    parser.add_argument("--flush", action="store_true",
            help="flush each message")

    parser.add_argument("--sleep", type=float, default=0,
            help="sleep time")

    parser.add_argument("--batch", type=int, default=1,
                        help="batch size for sending")

    parser.add_argument("--rate", type=int, default=-1,
                        help="sending rate")

    parser.add_argument("--append_timestamp", action="store_true",
            help="append timestamp to data with backtick separator")

    options = parser.parse_args()
    host=options.kafka.split(":")[0]
    port=options.kafka.split(":")[1]
    main(options, host=host, port=port)
