#!/usr/bin/env python3

import argparse
import numpy as np
import os
import time
import sys
import datetime
from scipy import stats
from kafka import KafkaProducer, KafkaConsumer
import subprocess
from sklearn.metrics import silhouette_score

HEADER_END_TOKEN="END"

toolbar_width = 40

def update_progress(progress):
    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


def header_strip(data):
    p = data.find(HEADER_END_TOKEN)
    if p == -1:
        return None
    return data[p + len(HEADER_END_TOKEN):]

def csv_to_list(filename):
    progress_parts = 20
    if not filename:
        return None
    """
    res = subprocess.run(['wc', '-l', filename], stdout=subprocess.PIPE)
    lines_num = int(res.stdout.decode("utf-8").strip().split(" ")[0])
    lines_num += 1
    progress_part_size = int(lines_num / progress_parts) - 1
    """

    ret = []
    print("Loading file: {}".format(filename))
    with open(filename) as f:
        i = 0
        for line in f:
            l = line.strip().split(",")
            ret.append([float(x) for x in l])
            """
            if i % progress_part_size == 0:
                update_progress(i/lines_num)
            i += 1
            """
    print("\n")
    return ret

def score(model, data):
    model = np.array(model)
    data = np.array(data)
    # TODO: Only 1000 samples are selected as it goes into memory error otherwise. We need a more systematic way to test here.
    data = data[1:1000,:]
    nr_centroids = len(model)
    nr_attributes = len(model[0])
    nr_samples = len(data)
    # TODO: Check that number of attributes is same as the model, else throw an error
    # nr_attributes_model = len(data[0])

    distance = np.zeros((nr_samples,nr_centroids))
    for a in range(0,nr_centroids):
        distance[:,a] = np.sum(np.square(data-model[a]),axis=1)
    labels = np.argmin(distance,axis=1)

    score = silhouette_score(data, labels, metric='euclidean')
    print("Model Score", score)
    print(model, nr_centroids, nr_attributes)


# returns model as list of lists of double
def cons_record_to_kmeans_model(cons_rec):
    value = cons_rec.value
    value = value.decode("utf-8")
    value = header_strip(value)
    value = value.strip()
    value = value.split('\n')
    for i, l in enumerate(value):
        value[i] = l.split(",")
        value[i][:] = [float(x) for x in value[i]]
    return value

def main(host=None, port=None, topic=None, filename=None):
    data = None
    if filename:
        data = csv_to_list(filename)
        print("Data file read: {},{}".format(len(data), len(data[0])))
    consumer = KafkaConsumer(topic, bootstrap_servers=["{}:{}".format(host, port)])
    for consumer_record in consumer:
        model = cons_record_to_kmeans_model(consumer_record)
        score(model, data)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--kafka", required=True,
            help="host:port")

    parser.add_argument("--topic", required=True,
            help="topic")

    parser.add_argument("--data_file",
            help="topic")

    options = parser.parse_args()
    host=options.kafka.split(":")[0]
    port=options.kafka.split(":")[1]
    main(host=host, port=port, topic=options.topic, filename=options.data_file)
