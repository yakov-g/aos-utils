#!/usr/bin/env python3

import argparse
import numpy as np
import os
import time
import datetime
from scipy import stats
from kafka import KafkaProducer

# DATA_FLOW options (DO NOT CHANGE)
CONSTANT_DATA_FLOW = 0
POISSON_DATA_FLOW = 1
LINEAR_DATA_FLOW = 2

# The interval rate of sending data with no sleep. Manually obtained this number.
MIN_DATA_INTERVAL_THRESHOLD = 0.00016

# OnlineKmeans/Kafka Parameters
APPEND_TIMESTAMP=False
ATTRIBUTES = 14

# Data-Flow Parameters
DATA_FLOW = CONSTANT_DATA_FLOW
SAMPLES_PER_ITER = 10000
MIN_DATA_INTERVAL = .00000033

# Poisson Parameters
MAX_RANGE = 10
MU = 5
SCALAR = 1.0

PM_PROD = None

class _TinyProducer:
    def __init__(self, serv=None, port=None, topic=None, prepend_label=False, flush=False):
        self._kafka_producer = KafkaProducer(bootstrap_servers="{}:{}".format(serv, port), acks=0, client_id=None)
        self._topic = topic
        self._prepend_label = prepend_label
        self._flush = flush

    def send(self, _msg, topic=None):
        msg = _msg
        send_to_topic = topic
        if self._prepend_label:
            msg = str("1;").encode("utf-8") + msg

        if send_to_topic is None:
            send_to_topic = self._topic
        self._kafka_producer.send(send_to_topic, msg)
        if self._flush:
            self._kafka_producer.flush()

class PMProducer:
    def __init__(self, flush=False):
        self._tiny_producers = []
        self._flush = flush

    def kafka_prod_add(self, serv=None, port=None, topic=None, prepend_label=False):
        tp = _TinyProducer(serv, port, topic, prepend_label, flush=self._flush)
        self._tiny_producers.append(tp)

    def send(self, msg, topic=None):
        for tp in self._tiny_producers:
            tp.send(msg, topic=topic)

def gettime():
    return (datetime.datetime.now() - datetime.datetime(1970,1,1)).total_seconds() + 8*60*60
    #return (datetime.datetime.now(tzlocal()) - datetime.datetime(1970,1,1, tzinfo=tzlocal())).total_seconds()

def generateVector():
    vector_str = ','.join(['%.3f' % num for num in np.random.rand(ATTRIBUTES)])
    if (APPEND_TIMESTAMP):
        vector_str += '`'
    return vector_str.encode("utf-8")

def generateData(samples):
    print("Generating data...")
    vector_strs = [None] * samples
    for i in range(samples):
        vector_strs[i] = generateVector()

    return vector_strs

def iterKafkaInputNoSleep(vector_strs):
    start_time = gettime()
    samp_time = 0
    count = 0
    interval = 10000
    if (APPEND_TIMESTAMP):
        for vec_str in vector_strs:
            PM_PROD.send(vec_str + str(int(time.time() * 1000)).encode("utf-8"))
            count += 1
            if count % interval == 0:
                if samp_time != 0.0:
                    print("Time: {}".format(datetime.datetime.now().time()))
                    curr_time = gettime()
                    rate = interval  / (curr_time - samp_time)
                    print("     Interval rate: {0} samples / sec".format(round(rate)))
                    print("  Global mean rate: {0} samples / sec".format(round(count / (curr_time - start_time))))
                samp_time = gettime()
    else:
        for vec_str in vector_strs:
            PM_PROD.send(vec_str)

def iterKafkaInputSleep(vector_strs, sleep_time):
    if (APPEND_TIMESTAMP):
        for vec_str in vector_strs:
            PM_PROD.send(vec_str + str(int(gettime() * 1000)).encode("utf-8"))
            time.sleep(sleep_time)
    else:
        for vec_str in vector_strs:
            PM_PROD.send(vec_str)
            time.sleep(sleep_time)

def iterKafkaInput(vector_strs, sleep_time):
    feed_start = time.time()
    if sleep_time <= MIN_DATA_INTERVAL_THRESHOLD:
        iterKafkaInputNoSleep(vector_strs)
    else:
        iterKafkaInputSleep(vector_strs, sleep_time)

    feed_end = time.time()
    feed_duration = feed_end - feed_start
    sample_interval_us = (feed_duration / SAMPLES_PER_ITER) * 1e06
    print("Feed duration: {} sec".format(feed_duration))
    print("Fed Kafka %d samples with an average interval of %dus" % (len(vector_strs), round(sample_interval_us)))
    print("Mean Rate: {} samples / sec".format(SAMPLES_PER_ITER / feed_duration))

def main(infinite=False):
    global PM_PROD
    PM_PROD = PMProducer()
    PM_PROD.kafka_prod_add("daenerys-c19", "9092", "KMEANS_TRAINING", prepend_label=False)
    PM_PROD.kafka_prod_add("daenerys-c19", "9092", "KMEANS_INFERENCE", prepend_label=False)

    vector_strs = generateData(SAMPLES_PER_ITER)
    while 1:
        if DATA_FLOW == POISSON_DATA_FLOW:
            offset = stats.distributions.poisson.pmf(MU, MU) + MIN_DATA_INTERVAL
            print("Starting to feed Kafka generated data with a poisson data-flow interval")
            for i in range(MAX_RANGE):
                sleep_time = SCALAR * (offset - stats.distributions.poisson.pmf(i, MU))
                iterKafkaInput(vector_strs, sleep_time)

        elif DATA_FLOW == CONSTANT_DATA_FLOW:
            print("Starting to feed Kafka generated data with a constant data-flow interval")
            iterKafkaInput(vector_strs, MIN_DATA_INTERVAL)

        elif DATA_FLOW == LINEAR_DATA_FLOW:
            for scalar in np.arange(2.0, 0, -0.25):
                sleep_time = scalar * MIN_DATA_INTERVAL
                iterKafkaInput(vector_strs, sleep_time)
        print("Completed data generation")

        if not infinite:
            break

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--infinite", action="store_true",
                        help="infinite message feed")

    parser.add_argument("--flush", action="store_true",
                        help="flush producer after each send")

    options = parser.parse_args()
    main(infinite=options.infinite)
