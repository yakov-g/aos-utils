#!/bin/sh

# ------------------------------------------------------------
# CONFIGURATION
# ------------------------------------------------------------

LOG_DIR=/net/home/jesse/flink/logs
KAFKA_DIR=/net/home/jesse/kafka_2.10-0.9.0.1
FLINK_DIR=/net/home/jesse/flink/build-target

KAFKA_NODE=daenerys-c23
KAFKA_PORT=2181
KAFKA_TOPIC=SVM
KAFKA_PARTITIONS=1

# ------------------------------------------------------------
# START AND STOP FUNCTIONS
# ------------------------------------------------------------

stop_zookeeper()
{
    (cd $KAFKA_DIR && ./bin/zookeeper-server-stop.sh &>> ${LOG_DIR}/zookeeper.log) &
    sleep 4
    echo "Started Zookeeper Server" 
}

stop_kafka_server()
{
    (cd $KAFKA_DIR && ./bin/kafka-server-stop.sh &>> ${LOG_DIR}/kafka_server.log) &
    sleep 4
    echo "Stopped Kafka Server"
}
delete_kafka_topic()
{
    (cd $KAFKA_DIR && ./bin/kafka-topics.sh --delete --topic ${KAFKA_TOPIC} --zookeeper ${KAFKA_NODE}:${KAFKA_PORT} &>> ${LOG_DIR}/kafka_topic.log) &
    sleep 4
    echo "Deleted Kafka Topic"
}
stop_flink_cluster()
{
    (cd $FLINK_DIR && ./bin/stop-cluster.sh &>> ${LOG_DIR}/flink_cluster.log) &
    sleep 4
    echo "Stopped Flink cluster"  
}

# ------------------------------------------------------------
# RUN ON EXECUTION
# ------------------------------------------------------------

stop_flink_cluster
delete_kafka_topic
stop_kafka_server
stop_zookeeper    
echo "Successfully exited"
exit

