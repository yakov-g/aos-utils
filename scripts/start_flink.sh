#!/bin/sh

# ------------------------------------------------------------
# CONFIGURATION
# ------------------------------------------------------------
LOG_DIR=/net/home/jesse/flink/logs
KAFKA_DIR=/net/home/jesse/kafka_2.11-0.10.0.1
FLINK_DIR=/net/home/jesse/flink/build-target
JAR_DIR=/net/home/jesse/flink/flink/flink-examples/flink-examples-streaming/target

KMEANS_JAR=${JAR_DIR}/OnlineKmeans.jar
SVM_JAR=${JAR_DIR}/OnlineSVM.jar

KAFKA_NODE=daenerys-c23
ZOOKEEPER_PORT=2181
KAFKA_PORT=9092
KAFKA_TOPIC=SVM
KAFKA_PARTITIONS=48

TO_RUN="${FLINK_DIR}/bin/flink run -p 48 "

# Runs SVM Inference using a Kafka Config. You can verify it's working by checking the taskmanager logs for debug output.
#TO_RUN+="${SVM_JAR} --kafkaConsumerConfigPath /net/home/jesse/flink/flink/build-target/consumer.properties --debug true --attributes 5 --topic ${KAFKA_TOPIC} --modelInit provided --modelPath /net/home/jesse/flink/flink/build-target/tmp.txt --mode inference --test false --dataSeparator comma_sep"

# Runs SVM CoFlatMap training using a Kafka Config. You can verify it's working by checking the taskmanager logs for debug output.
#TO_RUN+="${SVM_JAR} --kafkaConsumerConfigPath /net/home/jesse/flink/flink/build-target/consumer.properties --debug true --soft 2.0 --step 1.0 --attributes 5 --syncFrequencyCount 15 --gammaFreq 10 --topic ${KAFKA_TOPIC} --gamma 1.0 --modelInit provided --modelPath /net/home/jesse/flink/flink/build-target/tmp.txt --mode coflatmap --test false --dataSeparator comma_sep --labelDataSeparator semicolon_sep --maxWaitMillis 0"

# Runs Kmeans Inference using a Kafka Config. You can verify it's working by checking the taskmanager logs for debug output.
#TO_RUN+="${KMEANS_JAR} --kafkaConsumerConfigPath /net/home/jesse/flink/flink/build-target/consumer.properties --debug true --k 4 --attributes 6 --topic ${KAFKA_TOPIC} --modelInit rand --mode inference --test false --dataSeparator comma_sep --maxWaitMillis 0"

# Runs Kmeans CoFlatMap training to obtain latency numbers.
TO_RUN+="${KMEANS_JAR} --kafkaHostname ${KAFKA_NODE} --kafkaPort ${KAFKA_PORT} --debug false --k 4 --attributes 14 --topic ${KAFKA_TOPIC} --modelInit rand --mode coflatmap --syncFrequencyCount 50000 --test false --latency true --dataSeparator comma_sep --maxWaitMillis 0"

# Runs Kmeans Count Window training to obtain latency numbers.
#TO_RUN+="${KMEANS_JAR} --kafkaHostname ${KAFKA_NODE} --kafkaPort ${KAFKA_PORT} --debug false --k 4 --attributes 6 --topic ${KAFKA_TOPIC} --modelInit rand --mode window --syncFrequencyCount 500000 --test false --latency true --dataSeparator comma_sep --maxWaitMillis 0"

# Runs SVM Inference to obtain latency numbers.
#TO_RUN+="${SVM_JAR} --attributes 14 --performance true --kafkaHostname ${KAFKA_NODE} --kafkaPort ${KAFKA_PORT} --topic ${KAFKA_TOPIC} --modelInit provided --debug false --modelPath /net/home/jesse/fucking_svm/model14.txt --mode inference --test false --latency true --dataSeparator comma_sep"


# ------------------------------------------------------------
# START AND STOP FUNCTIONS
# ------------------------------------------------------------

start_zookeeper()
{
    (cd $KAFKA_DIR && ./bin/zookeeper-server-start.sh ./config/zookeeper.properties &>> ${LOG_DIR}/zookeeper.log) &
    sleep 8
    echo "Started Zookeeper Server" 
}

stop_zookeeper()
{
    (cd $KAFKA_DIR && ./bin/zookeeper-server-stop.sh &>> ${LOG_DIR}/zookeeper.log) &
    sleep 8
    echo "Started Zookeeper Server" 
}

start_kafka_server()
{
    (cd $KAFKA_DIR && ./bin/kafka-server-start.sh ./config/server.properties &>> ${LOG_DIR}/kafka_server.log) &
    sleep 8
    echo "Started Kafka Server" 
}

stop_kafka_server()
{
    (cd $KAFKA_DIR && ./bin/kafka-server-stop.sh &>> ${LOG_DIR}/kafka_server.log) &
    sleep 8
    echo "Stopped Kafka Server"
}

create_kafka_topic()
{
    (cd $KAFKA_DIR && ./bin/kafka-topics.sh --create --topic ${KAFKA_TOPIC} --zookeeper ${KAFKA_NODE}:${ZOOKEEPER_PORT} --partitions ${KAFKA_PARTITIONS} --replication-factor 1 &>> ${LOG_DIR}/kafka_topic.log) &
    sleep 8
    echo "Created Kafka Topic"
}

delete_kafka_topic()
{
    (cd $KAFKA_DIR && ./bin/kafka-topics.sh --delete --topic ${KAFKA_TOPIC} --zookeeper ${KAFKA_NODE}:${ZOOKEEPER_PORT} &>> ${LOG_DIR}/kafka_topic.log) &
    sleep 8
    echo "Deleted Kafka Topic"
}

start_flink_cluster()
{
    (cd $FLINK_DIR && ./bin/start-cluster.sh &>> ${LOG_DIR}/flink_cluster.log) &
    sleep 4
    echo "Started Flink cluster" 
}

stop_flink_cluster()
{
    (cd $FLINK_DIR && ./bin/stop-cluster.sh &>> ${LOG_DIR}/flink_cluster.log) &
    sleep 4
    echo "Stopped Flink cluster"  
}

on_exit() {
    trap '' INT TERM     # ignore INT and TERM while shutting down
    kill -9 $1
    sleep 2
    echo "Killed running process"
    stop_flink_cluster
    delete_kafka_topic
    stop_kafka_server
    stop_zookeeper
    sudo fuser -k ${KAFKA_PORT}/tcp
    sudo fuser -k ${ZOOKEEPER_PORT}/tcp
    kill -TERM 0         # fixed order, send TERM not INT
    wait
    echo "Successfully exited"
    exit
}

# ------------------------------------------------------------
# RUN ON EXECUTION
# ------------------------------------------------------------

start_zookeeper
start_kafka_server
create_kafka_topic
start_flink_cluster

eval ${TO_RUN} &>> ${LOG_DIR}/run.log &
TO_RUN_PID=$!
echo "Successfully executed"
echo ${TO_RUN}
echo "with PID ${TO_RUN_PID}"

trap 'on_exit ${TO_RUN_PID}' INT

while [ 1 ]
do
    sleep 3
done

